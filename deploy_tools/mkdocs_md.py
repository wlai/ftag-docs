HIDE_TOC = (
    "---",
    "hide:",
    "  - toc",
    "---",
)


class Markdown:
    """Class to write markdown for material for mkdocs."""

    def __init__(self, md: str = "", hide_toc=False) -> None:
        self.md = md
        if hide_toc:
            self.md += "\n".join(HIDE_TOC)

    def __add__(self, other):
        if not other.startswith("\n") and self.md != "":
            other = "\n" + other
        self.md += other
        return self

    def __radd__(self, other):
        if not self.md.startswith("\n\n") and other != "":
            self.md = "\n\n" + self.md
        return other + str(self)

    def __str__(self) -> str:
        return self.md

    def indent(self, text):
        return "\t" + text.replace("\n", "\n\t")

    def header(self, text, level):
        return "#" * level + " " + text + "\n"

    def paragraph(self, text):
        return text + "\n\n"

    def code(self, text, language="", line_numbers=False):
        ln = ""
        if line_numbers:
            ln += ' linenums="1"'
        return "\n\n```" + language + ln + "\n" + text + "\n```\n\n"

    def link(self, text, url):
        return "[" + text + "](" + url + ")"

    def image(self, text, url):
        return "![" + text + "](" + url + ")"

    def table(self, data: dict[str, list], format_ints=True):
        assert len({len(row) for row in data.values()}) == 1
        text = "| "
        for header in data:
            text += header + " | "
        text += "\n| "
        for header in data:
            text += "-" * len(header) + " | "
        text += "\n"

        for row_idx in range(len(list(data.values())[0])):
            text += "| "
            for col_idx in range(len(data.keys())):
                value = data[list(data.keys())[col_idx]][row_idx]
                if format_ints and (isinstance(value, str) and value.isdigit()) or isinstance(value, int):
                    value = f"{int(value):,}"
                text += str(value) + " | "
            text += "\n"

        text += "\n\n"
        return text

    def list(self, items, ordered=False):
        text = ""
        for item in items:
            text += "- " + item + "\n"
        text += "\n\n"
        return text

    def admonition(self, style, header, body=None, expanded=False):
        if body is None:
            expanded = False
        md = "\n"
        md += "!!!" if body is None else "???"
        if expanded:
            md += "+"
        md += " "
        md += style + ' "' + header + '"\n\n'
        if body:
            md += self.indent(body) + "\n\n"
        return md

    def sections(self, sections: dict[str, str]):
        text = "\n"
        for header, body in sections.items():
            text += "=== " + f'"{header}"' + " \n\n"
            text += self.indent(body) + "\n\n"
        return text

    def to_file(self, fname):
        with open(fname, "w") as f:
            f.write(self.md)
