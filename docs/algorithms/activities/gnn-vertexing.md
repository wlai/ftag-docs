# GNN Vertexing

The vertexing capabilities of the new graph-neural-network (GNN) algorithms, which use an auxiliary task for pairing tracks to secondary vertices to improve the discrimination between b-jets, c-jets and light-flavour jets.

A good description of the GNN taggers is provided in the PUB note which describes GN1: [ATL-PHYS-PUB-2022-027](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2022-027/).

The vertexing auxiliary training objective is the prediction of track-pair vertex compatibility.
For each pair of tracks in the jet, the network predicts a binary label, which is given a value 1 if the two tracks in the pair originated from the same point in space, and 0 otherwise. To derive the corresponding truth labels for training, truth production vertices within 0.1 mm are merged. Track-pairs where one or both of the tracks in the pair have an origin label of either Pileup or Fake are given a label of 0. Using the pairwise predictions from the model, collections of commonly compatible tracks can be grouped into vertices. The addition of this auxiliary training objective removes the need for inputs from a dedicated secondary vertexing algorithm.

More concretely, the `VertexFinder` takes a pair of nodes in the graph and  outputs a binary label to say whether these nodes are in the same vertex. This is done using an [union-find algorithm](https://en.wikipedia.org/wiki/Disjoint-set_data_structure). The algorithm which is used is ["Connected-component labeling"](https://en.wikipedia.org/wiki/Connected-component_labeling).

From the track-pair vertex prediction tracks can be partitioned into compatible groups representing vertices. As such, the network is able to be used to perform vertex “finding”, but not vertex “fitting”, i.e. the reconstruction of a vertex’s properties, which currently still requires the use of a dedicated vertex fitter.

The development of such a vertex fitter is a work package and should be the next goal for development of an improved version of the GNN tagger.



## Collaboration with tracking CP group

The tracking CP group is interested in using the Umami + Salt infrastructure to improve vertexing algorithms based on GNNs.

The current implementation of the vertex auxiliary task in the GNN tagger is only capable of performing vertex finding, but does not attempt to fit the identified groups of tracks to a common vertex. 
A combined authorship qualification project has been defined:
The goal of the qualification project is to develop a tool to perform this vertex fit, and study its performance in comparison to existing secondary vertex reconstruction methods. The tool should be flexible enough to facilitate generic vertex reconstruction from a GNN-based vertex finder.

- Jira ticket: [ATLIDTRKCP-455](https://its.cern.ch/jira/browse/ATLIDTRKCP-455)
- Mattermost chat: [private mattermost chat](https://mattermost.web.cern.ch/atlas/channels/tvpdftag-qp-sv-reconstruction-using-gnns)


## Implementation of vertex fitting

Link to [codimd document](https://codimd.web.cern.ch/JDCCdAS4SDmeRJsQCPBjFQ) with discussion on how to implement vertex fitting.

**Task list:**

- Decide whether to base targets on `TruthVertex` or `TruthParticle` objects.
- Study whether we need to merge nearby decays. If so, determine the approrpriate merging distance.
- Make sure the relevant targets (and links to tracks) are available in h5 via modifications to Athena/TDD.
- Implement vertex regression in salt (progressively more sophisticated options):
    1. Regression assuming the vertex cardinality, using all tracks in the jet (this is equivalent to just adding a auxiliary regression task and is supported with minimal code changes) 
    2. Regression assuming the vertex cardinality, using tracks with predicted origin fromB (or fromBC/fromC)
    3. Predict the adjacency matrix from the compatible track pairs and use this to determine connected components. Use these to merge representations of the relevant tracks. A bit more advanced as you need to do bipartate matching for the loss etc.




## Vertexing performance

The performance of the vertex finding is determined based on the truth vertex label of the tracks.
The inclusive vertex finding efficiency is estimated using inclusive truth decay vertices from b-hadron decays and comparing that to the vertices with tracks identified as coming from b-hadrons found by the GNN and merged together.

Vertices are compared with the target truth vertex and the number of correctly and incorrectly assigned tracks is computed. A vertex is considered matched if it contains at least 65% of the tracks in the corresponding truth vertex, and has a purity of at least 50%. 

**GN1 performance**

GN1 manages to achieve an inclusive reconstruction efficiency in 𝑏-jets of ∼80%.

**GN2 performance**


??? info "Task: Quantify vertexing performance for GN2"


    The vertexing performance of GN2 should be compared w.r.t. that of GN1 to provide improved understanding of current baseline. 