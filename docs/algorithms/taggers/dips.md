# DIPS Tagger

The Deep Impact Parameter Sets (DIPS) tagger uses an architecture based on [Deep Sets](https://arxiv.org/abs/1703.06114), which is known within particle physics as the formalism of the [Energy / Particle Flow Networks](https://arxiv.org/abs/1810.05165) and treats all tracks of one jet as an unordered, variable-sized set rather than as a sequence to identify jets originating from heavy flavour decays.

In contrast to the previously adopted [Recurrent Neural Network Impact Parameter (RNNIP)](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2017-003/) approach which treats track collections as a sequence, the DIPS tagger comprises a permutation-invariant and highly parallelisable architecture. Consequentially, it offers a significant decrease in training and evaluation time. Furthermore, the permutation invariance encoded in the model is well motivated by the permutation invariance of tracks.

Dips is used as an input to the [DL1d](dl1.md) algorithm and is using impact parameter information of tracks.


## Deep Sets
The deep set architecture is based a set function _f_, which can be decomposed like this:   

$$
f(X) = \rho \left( \sum_{x\in X} \phi(x)\right)
$$

Each element of the set can be embedded with a mapping function $ϕ$. The resulting embedding can now be aggregated into an invariant description of the set (i.e with summation). Afterwards, the aggregated embedding can be processed by another function _ρ_.   
Because _ϕ_ and _ρ_ are functions which not operate on the set itself, but on its elements, we can plug in neural networks here. 
With this way of processing the sets, we also get rid of the need of an ordering of the elements, like for RNNIP.

The DIPS architecture is shown here:
![DIPS architecture](../../assets/dips_architecture.png)

The track input variables are first processed by two layers of 100 nodes and one layer of 128 nodes with ReLU activation and batch normalisation. The tracks are processed shared weights layers. This is realized by the [`TimeDistributed` layer from Keras](https://keras.io/api/layers/recurrent_layers/time_distributed/).   
After the first processing, the results from all tracks are concatenated and pooled by pooled by summing up the different tracks. The output, now with a fixed dimension regardless the number tracks of the jet, is now handed to another three layers of each 100 nodes with ReLU activation and batch normalisation. The final layer has 30 nodes, also with ReLU activation and batch normalisation, and outputs three class probabilities.   
These outputs describe the probability for the jet originating from the respective quark. The three classes are _b_, _c_ and light jets. A special class are the light jets, which consist of _u_-, _d_- and _s_-jets.

Different DIPS architectures have been studied. Their parameters are displayed in the table below.

| Hyperparameter | DIPS (ATL-PHYS-PUB-2020-014) | DIPS Default and DIPS Loose |
| -------------- | -------------------------------------- | --------------------------- |
| Aggregation function | Summation | Summation |
| Loss function | Categorical Crossentropy | Categorical Crossentropy
| Optimiser | ADAM (Adaptive Moment Estimation) | ADAM |
| Activation function | ReLU (Rectified Linear Unit) | ReLU |
| Output activation function | Softmax | Softmax |
| Regularisation | Batch Normalisation | Batch Normalisation |
| _ϕ_ number of hidden layers | 3 | 3 | 3 |
| _ϕ_ number of nodes per layer | [100, 100, 128] | [100, 100, 128] |
| _F_ number of hidden layers | 2 | 4 | 4 |
| _F_ number of nodes per layer | [100, 100] | [100, 100, 100, 30] |
| number of training jets | 3M | 22.8 M |
| batch size | 256 | 15000 |
| Free (trainable) parameters | 48987 | 62167 |
| Fixed parameters | 1056 | 1316 |


As the DIPS neural network provides a multi-node output, a b-tagging discriminant using these outputs can be defined as 

$$
D_b = \ln \left( \frac{p_b}{f_c\cdot p_b + (1-f_c)\cdot p_u}\right)
$$

where $f_c$ denotes a tunable parameter to optimise the _c_- and light jet rejection. The current default value for DIPS is $f_c = 0.005$.

## Documentation

- [Deep Sets based Neural Networks for Impact Parameter Flavour Tagging in ATLAS](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2020-014/)
- [DIPS optimisation in release 21](https://cds.cern.ch/record/2785114)

## Training of DIPS
DIPS itself is trained with a hybrid training sample of ttbar- and Z' jets. The Z' jets are used to give more statistics to higher pT jets. The Z' events for training are simulated with different masses to ensure jets with a pT range from 250 GeV to 6 TeV.   
The hybrid training sample contains 70% ttbar- and 30% Z' jets with a special kinematic shape.
To ensure a kinematic independent tagging, a grid of pT/eta bins is created. Each of these pT/eta bins is filled with the same amount of jets per flavour. Therefore, we achieve a kinematic independent tagging for each of the three classes. 
DIPS is trained using the [Umami Framework](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami). A detailed description how to train DIPS is given in the [documentation of Umami](https://umami-docs.web.cern.ch).


## DIPS Attention (CADS)
Although the performance of DIPS is already significantly better than the performance of the recommended RNNIP, studies have shown that due to the looser track selection the performance gain of DIPS in the high $p_T$ region is severely worse than in the lower $p_T$ region. This happens due to the nature of deep sets. In high $p_T$, the multiplicity of tracks originating from the decay of the _b_-hadron decreases while the number of tracks from fragmentation and pile-up increase. 

### Attention Sub-Network
The problem here with the deep sets is the aggregation of the tracks. After processing via the _ϕ_ network, the tracks are pooled using a simple summation. When we now have drastically more tracks from fragmentation and pile-up and less tracks from the _b_-hadron decay, the deep set network will be blinded with useless info about the non-_b_-hadron decay tracks.

To tackle this issue we introduce a new sub-network, the attention sub-network _A_ which can be seen here:
![Attention Sub-Network](../../assets/attention_block.png)

The attention network is from the base architecture similar to the _ϕ_ network with one very specific difference: The last layer. While the deep sets network _ϕ_ has 128 units with ReLU activation in the last layer, _A_ uses a layer with one unit and sigmoid activation. Per track we are getting one value between 0 and 1 for each track which can be interpreted as a weight for the track. When we add this new sub-network now to DIPS, we get DIPS Attention which can be seen here:
![DIPS Attention architecture](../../assets/dips_attention_architecture.png)

The new _A_ network will be added in parallel to the already existing _ϕ_ network. To incorporate now _A_ into the full pipeline of the network and make full use of the now established weights, we replace the plain summation as aggregation function with a weighted summation. This weighted summation can be seen here.
![Pooling/Aggregation of DIPS Attention](../../assets/weighted_sum.png)

The new aggregation function also sums over all the available tracks to create an input dimension invariant description of the available information, but this time the weights of the tracks from the _A_ network are multiplied with the output of the tracks from the _ϕ_ network before they're summed.
With this net attention sub-network, DIPS Attention learns by itself which of the tracks is important or not for the classification and also replaces the need to calculate and set these weights by hand (if you want to introduce this kind of weighting).
