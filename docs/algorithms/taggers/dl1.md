# DL1 Tagger

DL1 represents a suite of deep-learning-based high-level algorithms.
Generally speaking, this class of algorithms takes the output of the low-level algorithms as input features and processes them using a fully-connected deep neural network.

The various sets of input features define the algorithms which belong to the DL1 family.
The illustration below shows the relation between the low-level algorithms and the high-level algorithms.

![Overview of DL1 taggers and the input features from low-level algorithms which they use](../../assets/DL1-tagger-structure-updated.png)


## DL1

The DL1 algorithm is based on a deep feed-forward neural network trained using Kera with the Adams optimiser. The DL1 network has a multidimensional output corresponding to the probabilities for a jet to be a b-jet, a c-jet or a light-flavour jet.
The topology of the neural network output consists of a mixture of fully connected hidden and Maxout layers. Batch normalisation is added by default.
The architecture of the NN, the number of training epochs, the learning rates and training batch size are all optimised to maximise the b-tagging efficiency.

The input features of DL1 are

- jet kinematics: pt and abs(eta)
- IP2D/IP3D: likelihood ratio between different flavour jet hypotheses based on track impact parameters
- SV1: properties of secondary vertices reconstructed with SV1 low-level algorithm
- JetFitter: properties of secondary vertices reconstructed with JetFitter low-level algorithm
- JetFitter c-tagging: additional properties of secondary vertices reconstructed with JetFitter low-level algorithm used for identification of charm jets

The properties of the DL1 neural network are shown in the table below.

| property                                   | value                           |
| ------------------------------------------ | ------------------------------- |
| number of input variables                  | 28                              |
| number of hidden layers                    | 8                               |
| number of nodes [per layer]                | [78, 66, 57, 48, 36, 24, 12, 6] |
| number of Maxout layers                    | 3                               |
| position of Maxout layers                  | [1, 2, 6]                       |
| number of parallel layers per Maxout layer | 25                              |
| number of training epochs                  | 240                             |
| learning rate                              | 0.0005                          |
| training minibatch size                    | 500                             |

The network provides multiple output nodes $p_b$, $p_c$, and $p_u$. Consequentially, there is some flexibility in the construction of the final output discriminant. The trained network can be used both for b-tagging and for c-tagging. 

The output discriminant used for b-tagging is defined as 

$$
D_{DL1} = \log\frac{p_b}{f_c \cdot p_c + (1-f_c)\cdot p_u},
$$

where $f_c$ represents the effective c-jet fraction in the background training sample.

## Documentation

- A description of the DL1 tagger and its performance is provided in [Eur. Phys. J. C 79 (2019) 970](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/FTAG-2018-01/).
- Internal documentation of the DL1 algorithm is provided here: [DL1 documentation](https://cds.cern.ch/record/2290144).


## DL1d

The DL1d algorithm is based on a deep feed-forward neural network, similar to the DL1 algorithm. It uses the [Deep Impact Paramet Set sub-tagger](https://ftag.docs.cern.ch/algorithms/dips/) (DIPS) variables instead of the RNNIP variables. The [input features](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami/-/blob/master/umami/configs/DL1r_Variables_R22.yaml) (replacing RNNIP with DIPS) of DL1d in Release 22 are:

- jet kinematics: pt and abs(eta)
- IP2D/IP3D: likelihood ratio between different flavour jet hypotheses based on track impact parameters
- SV1: properties of secondary vertices reconstructed with SV1 low-level algorithm
- JetFitter: properties of secondary vertices reconstructed with JetFitter low-level algorithm
- JetFitter c-tagging: additional properties of secondary vertices reconstructed with JetFitter low-level algorithm used for identification of charm jets
- DIPS: the three output probabilities of the [DIPS](https://ftag.docs.cern.ch/algorithms/dips/)

The properties of the DL1d neural network are shown in the table below.

| property                                   | value                             |
| ------------------------------------------ | --------------------------------- |
| number of input variables                  | 44                                |
| number of hidden layers                    | 8                                 |
| number of nodes [per layer]                | [256, 128, 60, 48, 36, 24, 12, 6] |
| activation function of all layers          | ReLu                              |
| batch normalisation                        | all layers                        |
| number of training epochs                  | 300                               |
| learning rate                              | 0.001                             |
| learning rate schedule                     | True                              |
| training minibatch size                    | 15000                             |

## DL1r

The DL1r algorithm is based on a deep feed-forward neural network, similar to the DL1 algorithm.
In extension, it uses also [RNNIP](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2017-003/) input variables.

## DL1rmu

The DL1rmu algorithm is based on a deep feed-forward neural network, similar to the DL1 algorithm.
In extension, it uses both [RNNIP](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2017-003/) variables and [SMT](https://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PUBNOTES/ATL-PHYS-PUB-2017-013/) variables.
The development of DL1rmu is not pursued further because of the intrinsic difficulty of calibrating the algorithm, as it is difficult to find a selection with light-flavour jets enriched in leptons.


## Training of DL1
DL1 itself is trained with a hybrid training sample of ttbar and Z' jets. The Z' jets are used to give more statistics to higher pT jets. The Z' events for training are simulated with different masses to ensure jets with a pT range from 250 GeV to 6 TeV.   
The hybrid training sample contains 70% ttbar- and 30% Z' jets with a special kinematic shape.
To ensure a kinematic independant tagging, a grid of pT/eta bins is created. Each of these pT/eta bins is filled with the same amount of jets per flavour. Therefore, we achieve a kinematic independant tagging for each of the three classes. Tau jets can be included within the samples without decreasing the main classes statistics by using alternative resampling methods (PDF sampling or probability ratio, as detailed in the [preprocessing section](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami/-/blob/master/docs/preprocessing.md)).


DL1 is trained using the [Umami framework](https://gitlab.cern.ch/atlas-flavor-tagging-tools/algorithms/umami). A detailed description how train DL1 is given in the documentation of [Umami](https://umami-docs.web.cern.ch).