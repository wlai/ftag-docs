# Flavor Tagging Recommendations

!!! Tip "looking for release 21?"
    The release 21 recommendations are [summarized on an older twiki][tw]

As a physics analyst you might be interested in two things from the flavor tagging group:

1. [Expected algorithm performance in MC](algs/r22-preliminary.md).
2. [data/MC scale factors for analyses](calib/intro_for_analysts.md).

[tw]: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagCalibrationRecommendationsRelease21
