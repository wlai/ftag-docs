# Derivations

The FTAG group maintains several different derivation formats, notably FTAG1 which is used for algorithm training and various performance studies.

!!! info "More info about FTAG derivations in Athena can be found in the [software docs](https://ftag-sw.docs.cern.ch/DAOD/)"

Below you can find guides for requesting DAOD and AOD samples for use in FTAG.


## DAOD Production Requests

For derivation productions, such as FTAG1, you should open a ticket under [ATLFTAGDPD](https://its.cern.ch/jira/projects/ATLFTAGDPD).
Be sure to include the following information:

- Tag the FTAG derivation contacts.
- Which samples you are requesting and why (e.g. ttbar and Z' samples for training).
- Which derivation format (e.g. FTAG1).
- Which release you would like the production in (e.g. `24.0.x`) and the corresponding p-tag.
- A list of DSIDs, split up by MC campaign.

The [FTAG derivation contacts](https://ftag-sw.docs.cern.ch/DAOD/#ftag-derivation-contacts) will then respond to your request,

??? abstract "Template FTAG1 Request"

    Dear `<tag derivation contacts>`

    We would like to request FTAG1 derivations for `<samples>` in release `<24.0.x>` (`<p-tag>`).

    The lists of samples is below.

    *MC23:*
    ```
    mc23_13p6TeV.....
    mc23_13p6TeV.....
    mc23_13p6TeV.....
    ```

    Cheers,
    <name>

    tagging <interested parties>


## AOD Production Requests

For xAOD production requests(e.g. with new DSIDs or extension), you should in general create a ticket under [ATLMCPROD](https://its.cern.ch/jira/browse/ATLMCPROD), containing necessary information:
- Motivation
- DSID of requested samples. New joboptions should be registered **after validation** to get the DSID number.
- Number of events requested in each period (multiply of 10k)
- Special reconstruction/simulation requirement(specific `tag`) if any. Default will be fullsim with latest official tag.
- FTAG approved priority. Default is 2 means "normal".

??? abstract "Template"

    [Tutorial](https://indico.cern.ch/event/1385465/contributions/5824415/attachments/2804668/4896397/Status%20Summay%20of%20FTAG%20MC%20Request.pdf)

    Examples:

    [https://its.cern.ch/jira/browse/ATLMCPROD-10827](https://its.cern.ch/jira/browse/ATLMCPROD-10827)

    [https://its.cern.ch/jira/browse/ATLMCPROD-10844](https://its.cern.ch/jira/browse/ATLMCPROD-10844?jql=project%20%3D%20ATLMCPROD%20AND%20component%20%3D%20Flavour-Tagging)

    Feel free to leave black if you're not sure and `<FTAG_MC_contacts>` is happy to help with!

    Dear MC Production Team,

    We would like to make a request for `<samples>` samples for `<usage>` in FTAG group. The stats requested are MC20(..)+MC23(..) in total `<total_stats>`. The conveners agreed to proceed with submitting the request with priority `<priority>`.

    Physics process and explanation: ....

    Generators used: ....

    Production type: ....

    Link to validation plots or presentation: ....

    Athena release: ....

    Priority and justification: ....
 
    JOs(if any newly registered): ....

    Thank you,

    `<Your name>`

    `<FTAG_MC_contacts>`

    cc: `<interested_parties>`, `<FTAG_conveners>`
